#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QLabel>
#include "Xml_Dom.h"

using namespace std;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    void initialisation();
    static QString dateToString(QDate *date);
    static QDate stringToDate(QString date);
    void resetTables();
    void resetFormsEnfant();
    QDomElement automatismeChamps(QDomElement item, bool remplissageInterface);

private slots:
    void majListeEnfants();
    void majInfosEnfant();
    void majInfosVisite();
    void majInfosVisite_2();
    void ajouterEnfant();
    void majEnfant();
    void supprimerEnfant();
    void ajouterVisite();
    void majVisite();
    void supprimerVisite();
    void ajouterVisite_2();
    void majVisite_2();
    void supprimerVisite_2();
    void importation();
    void exportation();
    void ouvertureSite();
    void informations();
    void maj_allergiesAlimentaires_1();
    void maj_allergiesAlimentaires_2();
    void nomEnfantMajuscules();
    void changeState_marche();
    void changeSexe();
    void imprimerDossierInfirmier();
    void imprimerExamens();
    void imprimerPansements();
    void imprimerTraitements();
    void imprimerHospitalisation();
    void choisirPhotoEnfant();

private:
    Xml_Dom *xml_doc;
    Ui::MainWindow *ui;
    double version;

};

#endif // MAINWINDOW_H
