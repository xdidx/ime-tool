#include "Xml_Dom.h"
#include "mainwindow.h"
#include <iostream>
#include <QMessageBox>

using namespace std;

Xml_Dom::Xml_Dom()
{
    xmlFile = "IME_xml_doc.xml";
}

QStringList Xml_Dom::listeSections()
{
    QStringList *listeSections = new QStringList();
    QDomNodeList sections = getDomDocument().documentElement().elementsByTagName("section");

    if(sections.length()==0)
    {
        //Ajout par default car pas d'ajout possible
        QDomDocument *newDomDoc = new QDomDocument("test");
        QDomElement newNode = newDomDoc->createElement("section");
        newNode.setAttribute("id", "1");
        newNode.setAttribute("nom", "IME");
        ajouterElement("sections", newNode);

        newNode = newDomDoc->createElement("section");
        newNode.setAttribute("id", "2");
        newNode.setAttribute("nom", "SESSAD");
        ajouterElement("sections", newNode);
    }

    sections = getDomDocument().documentElement().elementsByTagName("section");
    for(uint i=0;i<sections.length();i++){
        QDomElement item = sections.item(i).toElement();
        listeSections->append(item.attribute("nom"));
    }

    return *listeSections;
}
QStringList Xml_Dom::listeMedecins()
{
    QStringList *listeMedecins = new QStringList();
    QDomNodeList medecins = getDomDocument().documentElement().elementsByTagName("medecin");

    if(medecins.length()==0)
    {
        //Ajout par default car pas d'ajout possible
        QDomDocument *newDomDoc = new QDomDocument("test");
        QDomElement newNode = newDomDoc->createElement("medecin");
        newNode.setAttribute("id", "1");
        newNode.setAttribute("nom", "Docteur Courouble");
        ajouterElement("medecins", newNode);

        newNode = newDomDoc->createElement("medecin");
        newNode.setAttribute("id", "2");
        newNode.setAttribute("nom", "Docteur Millot");
        ajouterElement("medecins", newNode);
    }

    medecins = getDomDocument().documentElement().elementsByTagName("medecin");
    for(uint i=0;i<medecins.length();i++){
        QDomElement item = medecins.item(i).toElement();
        listeMedecins->append(item.attribute("nom"));
    }

    return *listeMedecins;
}

QList<QListWidgetItem*> Xml_Dom::listeEnfants(QString section)
{
    QList<QListWidgetItem*> *listeEnfants = new QList<QListWidgetItem*>();
    QDomElement docElem = getDomDocument().documentElement();

    //QDomNodeList enfants = triQNode(docElem.elementsByTagName("enfants").item(0), "id", "int").childNodes();
    QDomNodeList enfants = docElem.elementsByTagName("enfants").item(0).childNodes();

    QString id_section = "";
    if(section!="")
        id_section=getFieldFromField("section", "id", section, "nom");

    for(uint i=0;i<enfants.length();i++){
        QDomElement item = enfants.item(i).toElement();
        if(section=="" || item.attribute("enfantSections") == id_section)
        {
            QListWidgetItem *test = new QListWidgetItem(item.attribute("enfantNom") +" "+ item.attribute("enfantPrenom"));
            if(!derniereVisiteInf(item.attribute("id")))
                test = new QListWidgetItem ( QIcon(":/ressources/warning.png"), item.attribute("enfantNom") +" "+ item.attribute("enfantPrenom"));
            listeEnfants->append(test);
        }
    }
    //listeEnfants->sort();
    return *listeEnfants;
}
QDomNodeList Xml_Dom::listePesees(QString enfant)
{
    QDomElement docElem = getDomDocument().documentElement();
    QDomDocument temporary("tempDoc");

    QString id_enfant = getFieldFromField("enfant", "id", enfant, "enfantNom_enfantPrenom");
    QDomNodeList Pesees = docElem.elementsByTagName("pesee");
    for(uint i=0;i<Pesees.length();i++)
    {
        QDomNode item = Pesees.item(i);
        if(item.toElement().attribute("id_enfant") == id_enfant){
            QDomElement newItem = temporary.createElement(item.nodeName());
            for(uint j=0;j<item.toElement().attributes().length();j++)
                newItem.setAttribute(item.attributes().item(j).nodeName(), item.toElement().attribute(item.attributes().item(j).nodeName()));

            temporary.appendChild(newItem);
        }

    }
    return temporary.childNodes();
}
QStringList Xml_Dom::listeVisites(QString enfant, QString id_medecin)
{
    QDomElement docElem = getDomDocument().documentElement();
    QStringList *listeVisitesString = new QStringList();

    QDomNodeList visites = docElem.elementsByTagName("visite");

    QString id_enfant = getFieldFromField("enfant", "id", enfant, "enfantNom_enfantPrenom");

    for(uint i=0;i<visites.length();i++)
    {
        QDomElement item = visites.item(i).toElement();
        if(item.attribute("id_enfant") == id_enfant && item.attribute("id_medecin") == id_medecin){
            listeVisitesString->append(item.attribute("date"));
        }

    }
    return *listeVisitesString;
}

QDomElement Xml_Dom::InfosVisite(QString enfant, QString dateHeure, QString id_medecin)
{
    QDomElement docElem = getDomDocument().documentElement();

    QString id_enfant = getFieldFromField("enfant", "id", enfant, "enfantNom_enfantPrenom");
    QDomNodeList Visites = docElem.elementsByTagName("visite");
    for(uint i=0;i<Visites.length();i++){
        QDomElement item = Visites.item(i).toElement();
        if(item.attribute("date") == dateHeure && item.attribute("id_enfant") == id_enfant && item.attribute("id_medecin") == id_medecin)
            return item;
    }

    QDomElement *test = new QDomElement();
    return *test;
}
QDomElement Xml_Dom::InfosEnfant(QString enfant)
{
    QDomElement docElem = getDomDocument().documentElement();

    QString id_enfant = getFieldFromField("enfant", "id", enfant, "enfantNom_enfantPrenom");
    QDomNodeList enfants = docElem.elementsByTagName("enfant");
    for(uint i=0;i<enfants.length();i++){
        QDomElement item = enfants.item(i).toElement();
        if(item.attribute("id") == id_enfant){
            return item;
        }
    }

    QDomElement *test = new QDomElement();
    return *test;
}

bool Xml_Dom::derniereVisiteInf(QString id_enfant)
{
    bool visiteRecente=false;
    QDomElement docElem = getDomDocument().documentElement();

    QDomNodeList visites = docElem.elementsByTagName("visite");
    for(uint i=0;i<visites.length();i++){
        QDomElement item = visites.item(i).toElement();
        if(item.attribute("id_enfant") == id_enfant && item.attribute("id_medecin") == "2"){

            QDate dateVisite = MainWindow::stringToDate(item.attribute("date"));
            QDate dateDeuxAns = QDate::currentDate().addYears(-1);
            if(dateVisite>dateDeuxAns)
            {
                visiteRecente=true;
                break;
            }
        }
    }

    return visiteRecente;
}

bool Xml_Dom::ajouterElement(QString noeud, QDomElement newNode)
{
    QDomDocument dom = getDomDocument();

    if(dom.documentElement().elementsByTagName(noeud).length()==0){
        QDomElement createdNode = dom.createElement(noeud);
        dom.documentElement().appendChild(createdNode);
    }

    QDomNode domNode = dom.documentElement().elementsByTagName(noeud).item(0);

    //Verifications et choix de l'ID (ID max + 1)
    int new_id =  1;
    for(uint i=0;i<domNode.childNodes().length();i++)
    {
        QDomElement item = domNode.childNodes().item(i).toElement();
        //choix ID
        if(new_id <= item.attribute("id").toInt())
            new_id = item.attribute("id").toInt()+1;

        //verifs
        if(noeud=="enfants" && newNode.attribute("enfantNom")==item.attribute("enfantNom") && newNode.attribute("enfantPrenom")==item.attribute("enfantPrenom"))
        {
            QMessageBox::warning(NULL,"Enfant vide d�j� pr�sent","Un enfant vide a d�j� �t� ajout� et n'a pas encore �t� mis � jour (Section "+getFieldFromField("section", "nom", item.attribute("enfantSections"), "id")+")");
            return true;
        }
        else if(noeud=="sections" && newNode.attribute("nom")==item.attribute("nom"))
        {
            QMessageBox::warning(NULL,"Section d�j� pr�sente","Une section avec le m�me nom est d�j� pr�sente");
            return true;
        }
        else if(noeud=="visites" && newNode.attribute("date")==item.attribute("date") && newNode.attribute("id_medecin")==item.attribute("id_medecin"))
        {
            QMessageBox::warning(NULL,"Visite d�j� pr�sente","Une visite � la m�me date est d�j� pr�sente");
            return true;
        }

    }
    newNode.setAttribute("id", new_id);

    //Ajout dans le node
    domNode.appendChild(newNode);

    return majFichier(dom);
}
bool Xml_Dom::majElement(QString noeud, QDomElement newNode)
{
    QDomDocument dom = getDomDocument();
    QDomNode domNode = dom.documentElement().elementsByTagName(noeud).item(0);

    int index_item_repalce = -1;

    for(uint i=0;i<domNode.childNodes().length();i++){
        QDomElement item = domNode.childNodes().item(i).toElement();
        if(item.attribute("id")==newNode.attribute("id")){
            index_item_repalce = i;
        }else{
            //verifs
            if(noeud=="enfants" && newNode.attribute("enfantNom")==item.attribute("enfantNom") && newNode.attribute("enfantPrenom")==item.attribute("enfantPrenom"))
            {
                QMessageBox::warning(NULL,"Enfant vide d�j� pr�sent","Un enfant vide a d�j� �t� ajout� et n'a pas encore �t� mis � jour (Section "+getFieldFromField("section", "nom", item.attribute("enfantSections"), "id")+")");
                return true;
            }
            else if(noeud=="sections" && newNode.attribute("nom")==item.attribute("nom"))
            {
                QMessageBox::warning(NULL,"Section d�j� pr�sente","Une section avec le m�me nom est d�j� pr�sente");
                return true;
            }
            else if(noeud=="visites" && newNode.attribute("date")==item.attribute("date"))
            {
                QMessageBox::warning(NULL,"Visite d�j� pr�sente","Une visite � la m�me date est d�j� pr�sente");
                return true;
            }
        }

    }
    if(index_item_repalce>=0)
        domNode.replaceChild(newNode,domNode.childNodes().item(index_item_repalce));
    else
        return false;

    return majFichier(dom);
}
bool Xml_Dom::supprimerElement(QString nomNoeud, QString texte, QString nomChamps1, QString nomChamps2)
{
    QDomDocument dom = getDomDocument();

    QDomNode node = dom.documentElement().elementsByTagName(nomNoeud).item(0);
    for(uint i=0;i<node.childNodes().length();i++){
        QDomElement item = node.childNodes().item(i).toElement();

        QString champ = item.attribute(nomChamps1);
        if(nomChamps2!="")
            champ+=" "+item.attribute(nomChamps2);

        if(champ==texte)
        {
            if(nomNoeud=="sections")
                supprimerEnfants(dom, "enfant", "enfantSections", item.attribute("id"));

            node.removeChild(node.childNodes().item(i));
            if(majFichier(dom))
                return true;
        }
    }
    return false;
}

QDomDocument Xml_Dom::supprimerEnfants(QDomDocument dom, QString noeudEnfant, QString colonne, QString idParent)
{
    QDomElement domElement = dom.documentElement();

    QDomNodeList nodeList = domElement.elementsByTagName(noeudEnfant);
    for(uint i=0;i<nodeList.length();i++){
        QDomElement item = nodeList.item(i).toElement();

        if(item.attribute(colonne)==idParent)
        {
            item.setAttribute(colonne,"0");
        }
    }

    return dom;
}

bool Xml_Dom::majFichier(QDomDocument dom, QString fileName)
{
    if(fileName=="")
        fileName=xmlFile;
    if (QFileInfo(fileName).suffix().isEmpty())
        fileName.append(".xml");

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        return false;

    QTextStream stream(&file);
    stream << dom.toString();

    file.close();

    return true;
}

QString Xml_Dom::getFieldFromField(QString tagName, QString fieldWanted, QString fieldValue, QString fieldName)
{
    QDomElement docElem = getDomDocument().documentElement();

    QString retour="";
    QDomNodeList tagsList = docElem.elementsByTagName(tagName);
    for(uint i=0;i<tagsList.length();i++){
        QDomElement item = tagsList.item(i).toElement();

        QString fieldsGivenValue="";
        QStringList fieldsGiven = fieldName.split("_");
        for(int i=0;i<fieldsGiven.length();i++)
        {
            fieldsGivenValue += item.attribute(fieldsGiven[i])+" ";
        }

        if(fieldsGivenValue.trimmed() == fieldValue.trimmed())
        {
            QStringList fieldsWanted = fieldWanted.split("_");
            for(int i=0;i<fieldsWanted.length();i++)
            {
                retour += item.attribute(fieldsWanted[i])+" ";
            }
            break;
        }
    }
    return retour.trimmed();
}

QDomDocument Xml_Dom::getDomDocument(QString fileName)
{
    if(fileName=="")
        fileName=xmlFile;

    QDomDocument dom;
    QDomNode node( dom.createProcessingInstruction( "xml", "version='1.0' encoding='ISO-8859-1'" ) );
    dom.appendChild( node );

    QDomElement rootNode = dom.createElement("root");

    QDomElement enfantsNode = dom.createElement("enfants");
    QDomElement sectionsNode = dom.createElement("sections");
    QDomElement peseesNode = dom.createElement("pesees");
    QDomElement visitesNode = dom.createElement("visites");
    rootNode.appendChild(enfantsNode);
    rootNode.appendChild(sectionsNode);
    rootNode.appendChild(peseesNode);
    rootNode.appendChild(visitesNode);

    dom.appendChild(rootNode);

    QFile file(fileName);

    if(!file.exists())
        majFichier(dom);

    if (!file.open(QIODevice::ReadOnly)){
        QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture du fichier");
        return dom;
    }
    if (!dom.setContent(&file)) {
        QMessageBox::warning(NULL,"Erreur","Fichier non compatible");
        file.close();
        majFichier(dom);
        QFile file2(fileName);
        file2.open(QIODevice::ReadOnly);
        dom.setContent(&file2);
        return dom;
    }
    file.close();



    return dom;
}
