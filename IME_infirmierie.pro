#-------------------------------------------------
#
# Project created by QtCreator 2013-06-14T22:17:31
#
#-------------------------------------------------

QT       += core gui xml printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = IME_infirmierie
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Xml_Dom.cpp

HEADERS  += mainwindow.h \
    Xml_Dom.h

FORMS    += mainwindow.ui

RESOURCES += \
    ressources.qrc
