#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Xml_Dom.h"
#include <QHBoxLayout>
#include <QListWidget>
#include <QMessageBox>
#include <QLabel>
#include <QFormLayout>
#include <QLineEdit>
#include <QGraphicsTextItem>
#include <QDateEdit>
#include <QTextBrowser>
#include <QFileDialog>
#include <QPrinter>
#include <QtGlobal>

using namespace std;

const QString listeAttributsEnfant[] = {"LE_enfantNom", "LE_enfantPrenom", "DE_enfantDateNaissance", "LE_enfantTelephone", "DE_enfantDateEntree", "CB_enfantSections", "CB_enfantSexe", "LE_enfantMereNom", "DE_enfantMereDateNaissance", "LE_enfantMereProfession", "LE_enfantMereTelephone", "TE_enfantMereAdresse", "LE_enfantPereNom", "DE_enfantPereDateNaissance", "LE_enfantPereProfession", "LE_enfantPereTelephone", "TE_enfantPereAdresse", "TE_enfantFraterie", "LE_adminCAM", "LE_adminSS", "LE_adminMutuelle", "LE_medecinNom", "LE_medecinTelephone", "TE_medecinAdresse", "CKB_alimNormal", "CKB_alimMixee", "CKB_alimMorceaux", "CKB_alimMoulinee", "CKB_alimSemiLiquide", "CKB_alimViandeHachee", "CKB_hydraEau", "CKB_hydraEauGel", "CKB_religieuxHallal", "CKB_religieuxPorc", "CKB_religieuxKasher", "CKB_troublesConstipation", "CKB_troublesReflux", "TE_alimComplements", "TE_alimRemarques", "TE_allergiesMedicamenteuses", "TE_allergiesAlimentaires","LE_enfantTerme", "LE_enfantPoidsNaissance", "LE_enfantTailleNaissance", "TE_enfantNatureHandicap", "TE_enfantTroublesAssocies", "TE_enfantAccompAnte", "TE_commentairesDossier", "TE_enfantLieuResidence","LE_enfantAgeMarche","CKB_enfantAntecedantConvulsion","LE_enfantAgeDebutEpilepsie","DE_enfantDateArretTraitEpi","TE_enfantContraception","DE_enfantDateRegles","RB_marche","RB_epilepsie","RB_traitementEnCours","RB_enfantMere","RB_enfantPere","CKB_enfantSemainier"};
const QString listeTableauxEnfant[] = {"T_dossierInfirmier", "T_dossierTraitements", "T_dossierExamens","T_dossierPansements","T_dossierHospitalisation"};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    version=1.05;

    ui->setupUi(this);

    xml_doc = new Xml_Dom();

    connect(ui->listeSections, SIGNAL(itemSelectionChanged()), this, SLOT(majListeEnfants()));
    connect(ui->listeEnfants, SIGNAL(itemSelectionChanged()), this, SLOT(majInfosEnfant()));
    connect(ui->listeVisites, SIGNAL(itemSelectionChanged()), this, SLOT(majInfosVisite()));
    connect(ui->listeVisites_2, SIGNAL(itemSelectionChanged()), this, SLOT(majInfosVisite_2()));

    connect(ui->RB_marcheOui, SIGNAL(toggled(bool)), this, SLOT(changeState_marche()));
    connect(ui->RB_marcheNon, SIGNAL(toggled(bool)), this, SLOT(changeState_marche()));

    connect(ui->CB_enfantSexe, SIGNAL(currentIndexChanged(int)), this, SLOT(changeSexe()));
    connect(ui->B_enfantPhoto, SIGNAL(clicked()), this, SLOT(choisirPhotoEnfant()));

    connect(ui->menuAction_importer, SIGNAL(triggered()), this, SLOT(importation()));
    connect(ui->menuAction_exporter, SIGNAL(triggered()), this, SLOT(exportation()));
    connect(ui->menuAction_quitter, SIGNAL(triggered()), this, SLOT(close()));
    connect(ui->menuAction_modification, SIGNAL(triggered()), this, SLOT(ouvertureSite()));
    connect(ui->menuAction_informations, SIGNAL(triggered()), this, SLOT(informations()));

    connect(ui->B_ajoutEnfant, SIGNAL(clicked()), this, SLOT(ajouterEnfant()));
    connect(ui->B_majEnfantInfos, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantAlim, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantDossier, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantDossierAntecedents, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantExamens, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantTraitements, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantPansements, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->B_majEnfantHospitalisation, SIGNAL(clicked()), this, SLOT(majEnfant()));
    connect(ui->Image_supprimer_enfants, SIGNAL(clicked ()), this, SLOT(supprimerEnfant()));

    connect(ui->B_ajoutVisite, SIGNAL(clicked()), this, SLOT(ajouterVisite()));
    connect(ui->B_majVisite, SIGNAL(clicked()), this, SLOT(majVisite()));
    connect(ui->Image_supprimer_visites, SIGNAL(clicked ()), this, SLOT(supprimerVisite()));
    connect(ui->B_ajoutVisite_2, SIGNAL(clicked()), this, SLOT(ajouterVisite_2()));
    connect(ui->B_majVisite_2, SIGNAL(clicked()), this, SLOT(majVisite_2()));
    connect(ui->Image_supprimer_visites_2, SIGNAL(clicked ()), this, SLOT(supprimerVisite_2()));

    connect(ui->B_impressionDossierInfirmier, SIGNAL(clicked()), this, SLOT(imprimerDossierInfirmier()));
    connect(ui->B_impressionDossierInfirmierAntecedents, SIGNAL(clicked()), this, SLOT(imprimerDossierInfirmier()));
    connect(ui->B_imprimerExamens, SIGNAL(clicked()), this, SLOT(imprimerExamens()));
    connect(ui->B_imprimerTraitements, SIGNAL(clicked()), this, SLOT(imprimerTraitements()));
    connect(ui->B_imprimerPansements, SIGNAL(clicked()), this, SLOT(imprimerPansements()));
    connect(ui->B_imprimerHospitalisation, SIGNAL(clicked()), this, SLOT(imprimerHospitalisation()));

    connect(ui->LE_enfantNom, SIGNAL(textChanged(QString)), this, SLOT(nomEnfantMajuscules()));

    connect(ui->TE_allergiesAlimentaires_2, SIGNAL(textChanged()), this, SLOT(maj_allergiesAlimentaires_1()));
    connect(ui->TE_allergiesAlimentaires, SIGNAL(textChanged()), this, SLOT(maj_allergiesAlimentaires_2()));

    ui->L_enfantID->setVisible(true);
    ui->L_visiteID->setVisible(true);

    initialisation();


    //Couleurs onglets
    QTabBar *tabBar = ui->onglets->findChild<QTabBar *>(QLatin1String("qt_tabwidget_tabbar"));
    tabBar->setStyleSheet("font-weight:bold;");
    QColor beige = QColor(255,193,161,255);
    tabBar->setTabTextColor(0,beige);
    tabBar->setTabTextColor(1,beige);
    tabBar->setTabTextColor(2,beige);
    tabBar->setTabTextColor(3,QColor(167,5,22,255));
    tabBar->setTabTextColor(4,QColor(229,203,6,255));
    tabBar->setTabTextColor(5,QColor(119,204,32,255));
    tabBar->setTabTextColor(6,QColor(239,115,148,255));
    tabBar->setTabTextColor(7,QColor(54,141,188,255));
    tabBar->setTabTextColor(8,QColor(232,123,42,255));

    /*
    //Automatisme DiD
    ui->listeSections->setCurrentItem(ui->listeSections->item(1));
    ui->listeEnfants->setCurrentItem(ui->listeEnfants->item(0));
    ui->B_imprimerHospitalisation->click();
    */
}

//M�thodes
void MainWindow::initialisation()
{
    //Mise � neuf des champs
    resetFormsEnfant();

    //Mise � jour des listes de sections
    QStringList listeSectionsRecup = xml_doc->listeSections();
    ui->listeSections->clear();
    ui->listeSections->addItems(listeSectionsRecup);
    ui->CB_enfantSections->clear();
    ui->CB_enfantSections->addItems(listeSectionsRecup);

    //Mise � jour des listes d'enfants
    ui->listeEnfants->clear();

    //Mise � jour des listes d'enfants et de medecins
    ui->Image_supprimer_enfants->setHidden(true);
    ui->Image_supprimer_visites->setHidden(true);
}

QString MainWindow::dateToString(QDate *date)
{
    QString jour = QString::number(date->day());
    if(jour.length()<2)
        jour="0"+jour;
    QString mois = QString::number(date->month());
    if(mois.length()<2)
        mois="0"+mois;
    QString annee = QString::number(date->year());

    return jour+"/"+mois+"/"+annee;
}
QDate MainWindow::stringToDate(QString date)
{
    QStringList dateSplit = date.split("/");
    QDate *retour = new QDate(2000,1,1);
    if(dateSplit.length()==3)
        retour = new QDate(dateSplit[2].toInt(),dateSplit[1].toInt(),dateSplit[0].toInt());
    return *retour;
}
void MainWindow::resetFormsEnfant()
{
    resetTables();

    //Mise � jour des infos du patient
    QDomElement enfantItem = xml_doc->InfosEnfant("");

    //Mse � jour g�n�rales
    automatismeChamps(enfantItem, true);

    //Mise � jour specifiques
    ui->L_enfantID->setText("0");
    ui->L_nomEnfant->setText("");

    ui->DE_dateVisite->setDate(QDate::currentDate());
    ui->TE_objetVisite->setText("");
    ui->listeVisites->clear();

    ui->onglets->setEnabled(false);

    ui->L_enfantPhoto->setPixmap(QPixmap(":/ressources/interogation.png"));
    ui->B_enfantPhoto->setEnabled(false);
}
void MainWindow::resetTables()
{

    //Tableaux enfant
    int taille_tableau = sizeof(listeTableauxEnfant)/sizeof(*listeTableauxEnfant);
    for(int i=0;i<taille_tableau;i++)
    {
        QString nomChamps = listeTableauxEnfant[i];
        QTableWidget *widget = this->findChild<QTableWidget*>(nomChamps);
        if(widget!=0)
        {
            for (int row = 0; row<widget->rowCount(); row++)
            {
                for (int column=0; column<widget->columnCount(); column++)
                {
                    QTextEdit *edit = new QTextEdit();
                    widget->setCellWidget(row, column, edit);
                }
            }
        }
        else
        {
            QMessageBox::information(NULL,"Erreur","Tableau non trouv�");
        }
    }

}

//Slots
void MainWindow::majListeEnfants()
{
    resetFormsEnfant();

    ui->Image_supprimer_enfants->setHidden(true);

    ui->listeEnfants->clear();
    QList<QListWidgetItem*> listeEnfantsRecup = xml_doc->listeEnfants(ui->listeSections->currentItem()->text());
    for(int i=0;i<listeEnfantsRecup.length();i++)
    {
        ui->listeEnfants->addItem(listeEnfantsRecup.at(i));
    }
}
void MainWindow::majInfosEnfant()
{
    resetFormsEnfant();
    if(ui->listeEnfants->selectedItems().length()>0){
        ui->onglets->setEnabled(true);

        //Affichage de la croix de suppression
        int posX = ui->listeEnfants->pos().x()+ui->listeEnfants->width()-ui->Image_supprimer_enfants->width()-1;
        int posY = ui->listeEnfants->pos().y()+(ui->listeEnfants->currentIndex().row()*17);
        ui->Image_supprimer_enfants->setHidden(false);
        ui->Image_supprimer_enfants->move(posX,posY);

        ui->Image_supprimer_visites->setHidden(true);

        //Mise � jour de la liste des visites
        ui->listeVisites->clear();
        ui->listeVisites->addItems(xml_doc->listeVisites(ui->listeEnfants->currentItem()->text(),"1"));

        ui->listeVisites_2->clear();
        ui->listeVisites_2->addItems(xml_doc->listeVisites(ui->listeEnfants->currentItem()->text(),"2"));


        //Mise � jour des infos du patient
        QDomElement enfantItem = xml_doc->InfosEnfant(ui->listeEnfants->currentItem()->text());

        //Mse � jour g�n�rales
        automatismeChamps(enfantItem, true);

        //Mise � jour specifiques
        ui->L_enfantID->setText(enfantItem.attribute("id"));
        ui->L_nomEnfant->setText(enfantItem.attribute("enfantNom")+" "+enfantItem.attribute("enfantPrenom")+" ("+enfantItem.attribute("enfantDateNaissance")+")");


        ui->L_enfantPhoto->setPixmap(QPixmap(":/ressources/interogation.png"));
        QDir directory(QDir::currentPath()+"/photosEnfants/");
        QStringList filesList = directory.entryList(QDir::Files);
        QString filePath;
        foreach(filePath, filesList)
        {
            QFileInfo info(filePath);
            QString baseName=info.baseName();
            if(baseName==ui->L_enfantID->text())
                ui->L_enfantPhoto->setPixmap(QPixmap(QDir::currentPath()+"/photosEnfants/"+filePath));
        }
        ui->B_enfantPhoto->setEnabled(true);

    }

}
void MainWindow::ajouterEnfant()
{
    QDomDocument *newDomDoc = new QDomDocument("test");
    QDomElement newNode = newDomDoc->createElement("enfant");
    newNode.setAttribute("enfantNom", "Nouvel");
    newNode.setAttribute("enfantPrenom", "Enfant");
    newNode.setAttribute("enfantDateNaissance", "01/01/2000");
    newNode.setAttribute("enfantSexe", "Masculin");

    if(ui->listeSections->selectedItems().length()==0)
        ui->listeSections->setCurrentRow(0);
    newNode.setAttribute("enfantSections", xml_doc->getFieldFromField("section", "id", ui->listeSections->selectedItems().at(0)->text(), "nom"));

    if(xml_doc->ajouterElement("enfants", newNode))
    {
        QModelIndex indexListe = ui->listeSections->currentIndex();
        ui->listeSections->setCurrentItem(ui->listeSections->currentItem(), QItemSelectionModel::Deselect);
        ui->listeSections->setCurrentIndex(indexListe);

        ui->listeEnfants->setCurrentItem(ui->listeEnfants->findItems("Nouvel Enfant", Qt::MatchWrap).at(0));
    }
    else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de l'enfant a �chou�");
}
void MainWindow::majEnfant()
{

    //Mse � jour g�n�rales
    QDomDocument *newDomDoc = new QDomDocument("test");
    QDomElement newNode = automatismeChamps(newDomDoc->createElement("enfant"), false);

    //Mise � jour specifiques
    newNode.setAttribute("id", ui->L_enfantID->text());

    if(xml_doc->majElement("enfants", newNode)){
        ui->listeSections->setCurrentItem(ui->listeSections->currentItem(), QItemSelectionModel::Deselect);
        ui->listeSections->setCurrentItem(ui->listeSections->findItems(xml_doc->getFieldFromField("section", "nom", newNode.attribute("enfantSections"),"id"), Qt::MatchWrap).at(0));
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->findItems(newNode.attribute("enfantNom")+" "+newNode.attribute("enfantPrenom"), Qt::MatchWrap).at(0));
    }else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de l'enfant a �chou�");

}
void MainWindow::supprimerEnfant()
{
    int reponse = QMessageBox::question(this, "Etes vous sur?", "Voulez vous vraiment supprimer cet enfant ?", QMessageBox::Yes | QMessageBox::No);
    if (reponse == QMessageBox::Yes){
        if(xml_doc->supprimerElement("enfants", ui->L_enfantID->text(), "id")){
            QModelIndex indexListe = ui->listeSections->currentIndex();
            ui->listeSections->setCurrentItem(ui->listeSections->currentItem(), QItemSelectionModel::Deselect);
            ui->listeSections->setCurrentIndex(indexListe);
        }else
            QMessageBox::information(NULL,"Erreur pendant la suppression","La suppression de l'enfant a �chou�");
    }
}

void MainWindow::majInfosVisite()
{
    if(ui->listeVisites->selectedItems().length()>0)
    {
        //Affichage de la croix de suppression
        int posX = ui->listeVisites->pos().x()+ui->listeVisites->width()-ui->Image_supprimer_visites->width()-1;
        int posY = ui->listeVisites->pos().y()+(ui->listeVisites->currentIndex().row()*17);
        ui->Image_supprimer_visites->setHidden(false);
        ui->Image_supprimer_visites->move(posX,posY);

        QDomElement visite = xml_doc->InfosVisite(ui->listeEnfants->currentItem()->text(), ui->listeVisites->currentItem()->text(), "1");

        ui->L_visiteID->setText(visite.attribute("id"));
        ui->DE_dateVisite->setDate(stringToDate(visite.attribute("date")));
        ui->TE_objetVisite->setText(visite.attribute("objet"));
    }
}
void MainWindow::ajouterVisite()
{
    QDomDocument *newDomDoc = new QDomDocument("test");

    QDomElement newNode = newDomDoc->createElement("visite");
    QDate date = QDate::currentDate();

    newNode.setAttribute("date", dateToString(&date));
    newNode.setAttribute("id_enfant", xml_doc->getFieldFromField("enfant", "id", ui->listeEnfants->currentItem()->text(), "enfantNom_enfantPrenom"));
    newNode.setAttribute("id_medecin", "1");

    if(xml_doc->ajouterElement("visites", newNode)){
        QModelIndex indexListe = ui->listeEnfants->currentIndex();
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
        ui->listeEnfants->setCurrentIndex(indexListe);

        ui->listeVisites->setCurrentItem(ui->listeVisites->findItems(dateToString(&date), Qt::MatchWrap).at(0));
    }else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de la visite a �chou�");
}
void MainWindow::majVisite()
{
    QDomDocument *newDomDoc = new QDomDocument("test");
    QDomElement newNode = newDomDoc->createElement("visite");
    newNode.setAttribute("id", ui->L_visiteID->text());
    QDate test = ui->DE_dateVisite->date();
    newNode.setAttribute("date", dateToString(&test));
    newNode.setAttribute("id_enfant", xml_doc->getFieldFromField("enfant", "id", ui->listeEnfants->currentItem()->text(), "enfantNom_enfantPrenom"));
    newNode.setAttribute("id_medecin", "1");
    newNode.setAttribute("objet", ui->TE_objetVisite->toHtml());

    if(xml_doc->majElement("visites", newNode)){
        QModelIndex indexListe = ui->listeEnfants->currentIndex();
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
        ui->listeEnfants->setCurrentIndex(indexListe);

        ui->listeVisites->setCurrentItem(ui->listeVisites->findItems(newNode.attribute("date"), Qt::MatchWrap).at(0));
    }else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de l'enfant a �chou�");

}
void MainWindow::supprimerVisite()
{
    int reponse = QMessageBox::question(this, "Etes vous sur?", "Voulez vous vraiment supprimer cette visite ?", QMessageBox::Yes | QMessageBox::No);
    if (reponse == QMessageBox::Yes){
        if(xml_doc->supprimerElement("visites", ui->L_visiteID->text(), "id")){
            QModelIndex indexListe = ui->listeEnfants->currentIndex();
            ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
            ui->listeEnfants->setCurrentIndex(indexListe);
        }else
            QMessageBox::information(NULL,"Erreur pendant la suppression","La suppression de la visite a �chou�");
    }
}

void MainWindow::majInfosVisite_2()
{
    if(ui->listeVisites_2->selectedItems().length()>0)
    {
        //Affichage de la croix de suppression
        int posX = ui->listeVisites_2->pos().x()+ui->listeVisites_2->width()-ui->Image_supprimer_visites_2->width()-1;
        int posY = ui->listeVisites_2->pos().y()+(ui->listeVisites_2->currentIndex().row()*17);
        ui->Image_supprimer_visites_2->setHidden(false);
        ui->Image_supprimer_visites_2->move(posX,posY);

        QDomElement visite = xml_doc->InfosVisite(ui->listeEnfants->currentItem()->text(), ui->listeVisites_2->currentItem()->text(), "2");

        ui->L_visiteID_2->setText(visite.attribute("id"));
        ui->DE_dateVisite_2->setDate(stringToDate(visite.attribute("date")));
        ui->TE_objetVisite_2->setText(visite.attribute("objet"));
    }
}
void MainWindow::ajouterVisite_2()
{
    QDomDocument *newDomDoc = new QDomDocument("test");

    QDomElement newNode = newDomDoc->createElement("visite");
    QDate date = QDate::currentDate();

    newNode.setAttribute("date", dateToString(&date));
    newNode.setAttribute("id_enfant", xml_doc->getFieldFromField("enfant", "id", ui->listeEnfants->currentItem()->text(), "enfantNom_enfantPrenom"));
    newNode.setAttribute("id_medecin", "2");

    if(xml_doc->ajouterElement("visites", newNode)){
        QModelIndex indexListe = ui->listeEnfants->currentIndex();
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
        ui->listeEnfants->setCurrentIndex(indexListe);

        ui->listeVisites->setCurrentItem(ui->listeVisites_2->findItems(dateToString(&date), Qt::MatchWrap).at(0));
    }else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de la visite a �chou�");
}
void MainWindow::majVisite_2()
{
    QDomDocument *newDomDoc = new QDomDocument("test");
    QDomElement newNode = newDomDoc->createElement("visite");
    newNode.setAttribute("id", ui->L_visiteID_2->text());
    QDate test = ui->DE_dateVisite_2->date();
    newNode.setAttribute("date", dateToString(&test));
    newNode.setAttribute("id_enfant", xml_doc->getFieldFromField("enfant", "id", ui->listeEnfants->currentItem()->text(), "enfantNom_enfantPrenom"));
    newNode.setAttribute("id_medecin", "2");
    newNode.setAttribute("objet", ui->TE_objetVisite_2->toHtml());

    if(xml_doc->majElement("visites", newNode)){
        QModelIndex indexListe = ui->listeEnfants->currentIndex();
        ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
        ui->listeEnfants->setCurrentIndex(indexListe);

        ui->listeVisites_2->setCurrentItem(ui->listeVisites_2->findItems(newNode.attribute("date"), Qt::MatchWrap).at(0));
    }else
        QMessageBox::information(NULL,"Erreur pendant l'ajout","L'ajout de l'enfant a �chou�");

}
void MainWindow::supprimerVisite_2()
{
    int reponse = QMessageBox::question(this, "Etes vous sur?", "Voulez vous vraiment supprimer cette visite ?", QMessageBox::Yes | QMessageBox::No);
    if (reponse == QMessageBox::Yes){
        if(xml_doc->supprimerElement("visites", ui->L_visiteID_2->text(), "id")){
            QModelIndex indexListe = ui->listeEnfants->currentIndex();
            ui->listeEnfants->setCurrentItem(ui->listeEnfants->currentItem(), QItemSelectionModel::Deselect);
            ui->listeEnfants->setCurrentIndex(indexListe);
        }else
            QMessageBox::information(NULL,"Erreur pendant la suppression","La suppression de la visite a �chou�");
    }
}

void MainWindow::imprimerDossierInfirmier()
{
    QString fileName = "dossier_infirmier.pdf";

    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPageSize(QPrinter::A4);
        printer.setOrientation(QPrinter::Portrait);
        printer.setOutputFileName(fileName);
        printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

        QRect pageSize = QRect(0,0,printer.width(),printer.height());

        QPainter painter(&printer);
        QFont oldFont = painter.font();
        //oldFont.setPixelSize(150);
        QFont newFont = oldFont;
        newFont.setPixelSize(150);
        painter.setFont(newFont);

        //page1
        painter.drawImage(pageSize, QImage(":/ressources/pageDossier1.png"));

        painter.drawImage(QRect(7630,1620,1620,1920),QImage(ui->L_enfantPhoto->pixmap()->toImage()));

        painter.drawText(QRect(1900,2300,2800,200),ui->LE_enfantNom->text());
        painter.drawText(QRect(5700,2300,1700,200),ui->LE_enfantPrenom->text());
        QDate dateAffi = ui->DE_enfantDateNaissance->date();
        painter.drawText(QRect(3000,3000,2000,200),dateToString(&dateAffi));

        painter.drawText(QRect(3700,3800,5400,530),ui->TE_enfantLieuResidence->toPlainText());
        painter.drawText(QRect(2200,4530,5000,200),ui->LE_enfantTelephone->text());

        //Infos Pere
        painter.drawText(QRect(2500,5670,2500,200),ui->LE_enfantPereNom->text());
        dateAffi = ui->DE_enfantPereDateNaissance->date();
        painter.drawText(QRect(1800,5920,3000,200),dateToString(&dateAffi));
        painter.drawText(QRect(1950,6180,3100,900),ui->TE_enfantPereAdresse->toPlainText());
        painter.drawText(QRect(1650,7170,3100,200),ui->LE_enfantPereTelephone->text());
        painter.drawText(QRect(2200,7560,2700,200),ui->LE_enfantPereProfession->text());

        QRect rectStatutPere = QRect(4940,7960,200,200);
        if(ui->RB_enfantPereCouple->isChecked())
            rectStatutPere = QRect(4160,7960,200,200);
        if(ui->RB_enfantPereMarie->isChecked())
            rectStatutPere = QRect(3230,7960,200,200);
        painter.drawText(rectStatutPere,"x");

        //Infos Mere
        painter.drawText(QRect(5250,5920,3800,200),ui->LE_enfantMereNom->text());
        dateAffi = ui->DE_enfantMereDateNaissance->date();
        painter.drawText(QRect(5900,6170,3000,200),dateToString(&dateAffi));
        painter.drawText(QRect(6000,6430,3100,700),ui->TE_enfantMereAdresse->toPlainText());
        painter.drawText(QRect(5700,7200,3100,200),ui->LE_enfantMereTelephone->text());
        painter.drawText(QRect(6200,7610,2700,200),ui->LE_enfantMereProfession->text());

        QRect rectStatutMere = QRect(9050,8030,200,200);
        if(ui->RB_enfantMereCouple->isChecked())
            rectStatutMere = QRect(8240,8040,200,200);
        if(ui->RB_enfantMereMarie->isChecked())
            rectStatutMere = QRect(7370,8030,200,200);
        painter.drawText(rectStatutMere,"x");

        //Infos fraterie
        painter.drawText(QRect(1200,9160,8000,1100),ui->TE_enfantFraterie->toPlainText());

        //Infos Administration
        painter.drawText(QRect(3500,10900,5400,200),ui->LE_adminCAM->text());
        painter.drawText(QRect(1900,11270,7000,200),ui->LE_adminSS->text());
        painter.drawText(QRect(2000,11640,7000,200),ui->LE_adminMutuelle->text());

        //Infos Medecin traitant
        painter.drawText(QRect(1700,12370,3400,200),ui->LE_medecinNom->text());
        painter.drawText(QRect(5700,12370,3400,200),ui->LE_medecinTelephone->text());
        painter.drawText(QRect(2000,12740,7000,200),ui->TE_medecinAdresse->toPlainText());


        //page2
        printer.newPage();

        painter.drawImage(pageSize, QImage(":/ressources/pageDossier2.png"));

        QDate test = ui->DE_enfantDateEntree->date();
        painter.drawText(QRect(3460,1110,5110,220),dateToString(&test));
        painter.drawText(QRect(720,1870,8000,670),ui->TE_enfantAccompAnte->toPlainText());
        painter.drawText(QRect(1300,3550,2250,200),ui->LE_enfantTerme->text());
        painter.drawText(QRect(4120,3550,1800,200),ui->LE_enfantPoidsNaissance->text());
        painter.drawText(QRect(6580,3550,2100,200),ui->LE_enfantTailleNaissance->text());
        painter.drawText(QRect(680,4720,8050,570),ui->TE_enfantNatureHandicap->toPlainText());
        painter.drawText(QRect(700,5720,8020,310),ui->TE_enfantTroublesAssocies->toPlainText());
        painter.drawText(QRect(3800,6330,500,200),ui->LE_enfantAgeMarche->text());
        painter.drawText(QRect(3740,7920,3630,200),ui->LE_enfantAgeDebutEpilepsie->text());

        QRect marcheAcquise = QRect(5280,6340,200,200);
        if(ui->RB_marcheOui->isChecked())
            marcheAcquise = QRect(2540,6340,200,200);
        painter.drawText(marcheAcquise,"x");

        QRect epilepsie = QRect(2060,7520,200,200);
        if(ui->RB_epilepsieNon->isChecked())
            epilepsie = QRect(3230,7520,200,200);
        painter.drawText(epilepsie,"x");

        if(ui->CKB_enfantAntecedantConvulsion->isChecked())
            painter.drawText(QRect(5890,7520,200,200),"x");

        QRect traitementEnCours = QRect(4020,8140,200,200);
        if(ui->RB_traitementEnCoursNon->isChecked())
            traitementEnCours = QRect(4870,8140,200,200);
        painter.drawText(traitementEnCours,"x");

        QDate dateArret = ui->DE_enfantDateArretTraitEpi->date();
        painter.drawText(QRect(3700,8330,2000,200),dateToString(&dateArret));

        if(ui->CB_enfantSexe->currentText()=="Feminin")
        {
            QDate dateRegles = ui->DE_enfantDateRegles->date();
            painter.drawText(QRect(3000,8960,2000,200),dateToString(&dateRegles));
        }

        painter.drawText(QRect(2080,9320,6250,200),ui->TE_enfantContraception->toPlainText());
        painter.drawText(QRect(1060,10980,3270,690),ui->TE_allergiesMedicamenteuses->toPlainText());
        painter.drawText(QRect(5210,10980,3000,690),ui->TE_allergiesAlimentaires_2->toPlainText());

        //page3
        printer.newPage();
        painter.drawImage(pageSize, QImage(":/ressources/pageDossier3.png"));
        painter.drawText(QRect(2000,850,1300,500),ui->LE_enfantNom->text()+" "+ui->LE_enfantPrenom->text());
        painter.setFont(oldFont);
        //Mise en page du tableau
        for (int row = 0; row<ui->T_dossierInfirmier->rowCount(); row++)
        {
            for (int column=0; column<ui->T_dossierInfirmier->columnCount(); column++)
            {
                int cellHeight=1100;
                QRect cellSize = QRect(0,row*cellHeight+2100,0,0);
                if(column==0)
                {
                    cellSize = QRect(2150,cellSize.y(),1300,cellHeight-50);
                }
                else if(column==1)
                {
                    cellSize = QRect(3500,cellSize.y(),2700,cellHeight-50);
                }
                else if(column==2)
                {
                    cellSize = QRect(6200,cellSize.y(),2700,cellHeight-50);
                }


                QTextEdit *caseActu = (QTextEdit*)ui->T_dossierInfirmier->cellWidget(row, column);
                painter.drawText(cellSize,caseActu->toPlainText());
            }
        }

        //page4
        printer.newPage();
        painter.drawImage(pageSize, QImage(":/ressources/pageDossier4.png"));
        painter.drawText(QRect(750,680,8000,12000),ui->TE_commentairesDossier->toPlainText());

        painter.end();

        if(!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture");

    }
}
void MainWindow::imprimerExamens()
{
    QString fileName = "examens.pdf";

    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPageSize(QPrinter::A4);
        printer.setOrientation(QPrinter::Portrait);
        printer.setOutputFileName(fileName);
        printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

        QRect pageSize = QRect(0,0,printer.width(),printer.height());

        QPainter painter(&printer);
        QFont oldFont = painter.font();
        QFont newFont = oldFont;
        newFont.setPixelSize(200);
        painter.setFont(newFont);

        //page1
        int padding = 50;
        painter.drawImage(pageSize, QImage(":/ressources/pageExamens.png"));

        painter.drawText(QRect(1550,1260,3900,200),ui->LE_enfantNom->text());
        painter.drawText(QRect(2050,1560,3400,200),ui->LE_enfantPrenom->text());
        QDate dateAffi = ui->DE_enfantDateNaissance->date();
        painter.drawText(QRect(3150,1870,2300,200),dateToString(&dateAffi));

        painter.setFont(oldFont);
        for (int column=0; column<ui->T_dossierExamens->columnCount(); column++)
        {
            QRect cellSize = QRect(600+padding,2850+padding,1300-(2*padding),10350-(2*padding));
            if(column==1)
            {
                cellSize = QRect(1900+padding,2850+padding,2550-(2*padding),10350-(2*padding));
            }
            else if(column==2)
            {
                cellSize = QRect(4450+padding,2850+padding,2850-(2*padding),10350-(2*padding));
            }
            else if(column==3)
            {
                cellSize = QRect(7300+padding,2850+padding,2100-(2*padding),10350-(2*padding));
            }

            QTextEdit *caseActu = (QTextEdit*)ui->T_dossierExamens->cellWidget(0, column);
            painter.drawText(cellSize,caseActu->toPlainText());

        }

        painter.end();

        if(!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture");

    }
}
void MainWindow::imprimerTraitements()
{
    QString fileName = "traitements.pdf";

    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPageSize(QPrinter::A4);
        printer.setOrientation(QPrinter::Landscape);
        printer.setOutputFileName(fileName);
        printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

        QRect pageSize = QRect(0,0,printer.width(),printer.height());

        QPainter painter(&printer);
        QFont oldFont = painter.font();
        QFont newFont = oldFont;
        newFont.setPixelSize(200);
        painter.setFont(newFont);

        //page1
        int margin = 50;
        painter.drawImage(pageSize, QImage(":/ressources/pageTraitements.png"));

        painter.drawText(QRect(1300,1330,3900,200),ui->LE_enfantNom->text());
        painter.drawText(QRect(1800,1630,3400,200),ui->LE_enfantPrenom->text());
        QDate dateAffi = ui->DE_enfantDateNaissance->date();
        painter.drawText(QRect(2900,1960,2300,200),dateToString(&dateAffi));

        painter.setFont(oldFont);

        for (int column=0; column<ui->T_dossierTraitements->columnCount(); column++)
        {
            QRect cellSize = QRect(380+margin,2800+margin,1280-(margin*2),6270-(margin*2));
            if(column==1)
            {
                cellSize = QRect(1660+margin,2800+margin,2550-(margin*2),6270-(margin*2));
            }
            else if(column==2)
            {
                cellSize = QRect(4210+margin,2800+margin,2750-(margin*2),6270-(margin*2));
            }
            else if(column==3)
            {
                cellSize = QRect(6960+margin,2800+margin,2820-(margin*2),6270-(margin*2));
            }
            else if(column==4)
            {
                cellSize = QRect(9780+margin,2800+margin,3820-(margin*2),6270-(margin*2));
            }


            QTextEdit *caseActu = (QTextEdit*)ui->T_dossierTraitements->cellWidget(0, column);
            painter.drawText(cellSize,caseActu->toPlainText());

        }

        painter.end();

        if(!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture");

    }
}
void MainWindow::imprimerPansements()
{
    QString fileName = "pansements.pdf";

    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPageSize(QPrinter::A4);
        printer.setOrientation(QPrinter::Landscape);
        printer.setOutputFileName(fileName);
        printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

        QRect pageSize = QRect(0,0,printer.width(),printer.height());

        QPainter painter(&printer);
        QFont oldFont = painter.font();
        QFont newFont = oldFont;
        newFont.setPixelSize(200);
        painter.setFont(newFont);

        //page1
        int margin = 50;
        painter.drawImage(pageSize, QImage(":/ressources/pagePansements.png"));

        painter.drawText(QRect(1300,1110,3900,200),ui->LE_enfantNom->text());
        painter.drawText(QRect(1800,1380,3400,200),ui->LE_enfantPrenom->text());
        QDate dateAffi = ui->DE_enfantDateNaissance->date();
        painter.drawText(QRect(2900,1710,2300,200),dateToString(&dateAffi));

        painter.setFont(oldFont);

        for (int column=0; column<ui->T_dossierPansements->columnCount(); column++)
        {
            QRect cellSize = QRect(570+margin,2490+margin,1630-(margin*2),6500-(margin*2));
            if(column==1)
            {
                cellSize = QRect(2200+margin,2490+margin,3850-(margin*2),6500-(margin*2));
            }
            else if(column==2)
            {
                cellSize = QRect(6050+margin,2490+margin,3915-(margin*2),6500-(margin*2));
            }
            else if(column==3)
            {
                cellSize = QRect(10015+margin,2490+margin,3450-(margin*2),6500-(margin*2));
            }


            QTextEdit *caseActu = (QTextEdit*)ui->T_dossierPansements->cellWidget(0, column);
            painter.drawText(cellSize,caseActu->toPlainText());

        }

        painter.end();
        if(!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture");


    }
}
void MainWindow::imprimerHospitalisation()
{
    QString fileName = "hospitalisation.pdf";

    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setPageSize(QPrinter::A4);
        printer.setOrientation(QPrinter::Landscape);
        printer.setOutputFileName(fileName);
        printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);

        QRect pageSize = QRect(0,0,printer.width(),printer.height());

        QPainter painter(&printer);
        QFont oldFont = painter.font();
        QFont newFont = oldFont;
        newFont.setPixelSize(200);
        painter.setFont(newFont);

        //page1
        int margin = 50;
        painter.drawImage(pageSize, QImage(":/ressources/pageHospitalisation.png"));

        painter.drawText(QRect(1300,1280,3900,200),ui->LE_enfantNom->text());
        painter.drawText(QRect(1800,1580,3400,200),ui->LE_enfantPrenom->text());
        QDate dateAffi = ui->DE_enfantDateNaissance->date();
        painter.drawText(QRect(2900,1890,2300,200),dateToString(&dateAffi));

        painter.setFont(oldFont);

        for (int column=0; column<ui->T_dossierHospitalisation->columnCount(); column++)
        {
            QRect cellSize = QRect(350+margin,2900+margin,1600-(margin*2),6300-(margin*2));
            if(column==1)
            {
                cellSize = QRect(1950+margin,2900+margin,2800-(margin*2),6300-(margin*2));
            }
            else if(column==2)
            {
                cellSize = QRect(4750+margin,2900+margin,2200-(margin*2),6300-(margin*2));
            }
            else if(column==3)
            {
                cellSize = QRect(6950+margin,2900+margin,1500-(margin*2),6300-(margin*2));
            }
            else if(column==4)
            {
                cellSize = QRect(8450+margin,2900+margin,3450-(margin*2),6300-(margin*2));
            }
            else if(column==5)
            {
                cellSize = QRect(11900+margin,2900+margin,1750-(margin*2),6300-(margin*2));
            }


            QTextEdit *caseActu = (QTextEdit*)ui->T_dossierHospitalisation->cellWidget(0, column);
            painter.drawText(cellSize,caseActu->toPlainText());

        }

        painter.end();
        if(!QDesktopServices::openUrl(QUrl::fromLocalFile(fileName)))
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'ouverture");


    }
}

void MainWindow::importation()
{
    int reponse = QMessageBox::question(NULL,"Etes vous sur?","L'importation va remplacer les donn�es existantes par celles du fichier choisi. Etes vous sur de vouloir importer un fichier ?", QMessageBox::Yes | QMessageBox::No);
    if (reponse == QMessageBox::Yes){
        xml_doc->majFichier(xml_doc->getDomDocument(),"IME_back.xml");
        if(xml_doc->majFichier(xml_doc->getDomDocument(QFileDialog::getOpenFileName(0, QString(), QString(), "*.xml"))))
        {
            initialisation();
            QMessageBox::information(NULL,"Importation r�ussie","L'importation a bien �t� effectu�e");
        }
        else
        {
            QMessageBox::warning(NULL,"Erreur","Erreur pendant l'importation");
        }
    }
}
void MainWindow::exportation()
{
    if(xml_doc->majFichier(xml_doc->getDomDocument(), QFileDialog::getSaveFileName(0, QString(), QString(), "*.xml")))
    {
        QMessageBox::information(NULL,"Exportation r�ussie","L'exportation a bien �t� effectu�e");
    }else
    {
        QMessageBox::warning(NULL,"Erreur","Erreur pendant l'exportation");
    }
}
void MainWindow::ouvertureSite()
{
    QDesktopServices::openUrl(QUrl("http://did.sytes.net/IME/modifications.php"));
}
void MainWindow::informations()
{
    QMessageBox::information(NULL, "Information sur le logiciel", "IME infirmerie v"+QString::number(version)+"<br/><br/>Cr�� par Grard Rhandy<br/><a href='http://did.sytes.net/IME'>Site du logiciel</a><br/><br/><a href='http://did.sytes.net/IME/index.php?v="+QString::number(version)+"'>V�rifier les mises � jour</a>");
}

QDomElement MainWindow::automatismeChamps(QDomElement item, bool remplissageInterface)
{

    bool champsNonTrouve = false;
    int taille_tableau = sizeof(listeAttributsEnfant)/sizeof(*listeAttributsEnfant);
    for(int i=0;i<taille_tableau;i++)
    {

        champsNonTrouve = false;

        QString nomChamps = listeAttributsEnfant[i];
        QStringList champsDecoupe = nomChamps.split("_");

        if(champsDecoupe[0]=="DE")
        {
            QDateEdit *widget = this->findChild<QDateEdit*>(nomChamps);
            if(widget!=0)
            {
                if(remplissageInterface)
                    widget->setDate(stringToDate(item.attribute(champsDecoupe[1])));
                else{
                    QDate date_naissance = widget->date();
                    item.setAttribute(champsDecoupe[1], dateToString(&date_naissance));
                }
            }
            else
                champsNonTrouve=true;
        }
        else if(champsDecoupe[0]=="LE")
        {
            QLineEdit *widget = this->findChild<QLineEdit*>(nomChamps);
            if(widget!=0)
            {
                if(remplissageInterface)
                    widget->setText(item.attribute(champsDecoupe[1]));
                else
                    item.setAttribute(champsDecoupe[1], widget->text());
            }
            else
                champsNonTrouve=true;
        }
        else if(champsDecoupe[0]=="TE")
        {
            QTextEdit *widget = this->findChild<QTextEdit*>(nomChamps);
            if(widget!=0)
            {
                if(remplissageInterface)
                    widget->setText(item.attribute(champsDecoupe[1]));
                else
                    item.setAttribute(champsDecoupe[1], widget->toPlainText());
            }
            else
                champsNonTrouve=true;
        }
        else if(champsDecoupe[0]=="CKB")
        {
            QCheckBox *widget = this->findChild<QCheckBox*>(nomChamps);
            if(widget!=0)
            {
                if(remplissageInterface)
                    widget->setChecked(item.attribute(champsDecoupe[1])=="1");
                else
                    item.setAttribute(champsDecoupe[1], widget->isChecked());
            }
            else
                champsNonTrouve=true;
        }
        else if(champsDecoupe[0]=="CB")
        {
            QComboBox *widget = this->findChild<QComboBox*>(nomChamps);
            if(widget!=0)
            {
                if(remplissageInterface)
                {
                    QString texte = item.attribute(champsDecoupe[1]);
                    if(nomChamps=="CB_enfantSections")
                        texte = xml_doc->getFieldFromField("section", "nom", item.attribute("enfantSections"), "id");

                    int idx = (widget->findText(texte) == -1) ? 0 : widget->findText(texte);
                    widget->setCurrentIndex(idx);
                }
                else
                {
                    if(nomChamps=="CB_enfantSections")
                        item.setAttribute(champsDecoupe[1], xml_doc->getFieldFromField("section", "id", widget->currentText(), "nom"));
                    else
                        item.setAttribute(champsDecoupe[1], widget->currentText());
                }
            }
            else
                champsNonTrouve=true;
        }
        else if(champsDecoupe[0]=="RB")
        {
            if(nomChamps=="RB_enfantMere" || nomChamps=="RB_enfantPere")
            {
                QRadioButton *RB_Couple = this->findChild<QRadioButton*>(nomChamps+"Couple");
                QRadioButton *RB_Divorce = this->findChild<QRadioButton*>(nomChamps+"Divorce");
                QRadioButton *RB_Marie = this->findChild<QRadioButton*>(nomChamps+"Marie");
                if(remplissageInterface)
                {
                    RB_Couple->setChecked(item.attribute(champsDecoupe[1])=="Couple");
                    RB_Divorce->setChecked(item.attribute(champsDecoupe[1])=="Divorce" || item.attribute(champsDecoupe[1])=="");
                    RB_Marie->setChecked(item.attribute(champsDecoupe[1])=="Marie");
                }
                else
                {
                    if(RB_Couple->isChecked())
                        item.setAttribute(champsDecoupe[1],"Couple");
                    if(RB_Divorce->isChecked())
                        item.setAttribute(champsDecoupe[1],"Divorce");
                    if(RB_Marie->isChecked())
                        item.setAttribute(champsDecoupe[1],"Marie");
                }
            }
            else
            {
                QRadioButton *widgetOui = this->findChild<QRadioButton*>(nomChamps+"Oui");
                QRadioButton *widgetNon = this->findChild<QRadioButton*>(nomChamps+"Non");
                if(widgetOui!=0 && widgetNon!=0)
                {
                    if(remplissageInterface)
                    {
                        widgetOui->setChecked(item.attribute(champsDecoupe[1])=="1");
                        widgetNon->setChecked(item.attribute(champsDecoupe[1])=="0" || item.attribute(champsDecoupe[1])=="");
                    }
                    else
                        item.setAttribute(champsDecoupe[1], widgetOui->isChecked());
                }
                else
                    champsNonTrouve=true;
            }
        }
        else
        {
            QMessageBox::warning(NULL,"Oubli !","Oubli d'un type de champs : "+nomChamps);
        }

        if(champsNonTrouve)
            QMessageBox::warning(NULL,"Champs non trouv�","Champs non trouv� : "+nomChamps);

    }

    //Tableaux enfant
    taille_tableau = sizeof(listeTableauxEnfant)/sizeof(*listeTableauxEnfant);
    for(int i=0;i<taille_tableau;i++)
    {
        QString nomChamps = listeTableauxEnfant[i];
        QString nomTableau = nomChamps.split("_")[1];
        QTableWidget *widget = this->findChild<QTableWidget*>(nomChamps);
        if(widget!=0)
        {
            for (int row = 0; row<widget->rowCount(); row++)
            {
                for (int column=0; column<widget->columnCount(); column++)
                {
                    if(remplissageInterface)
                        ((QTextEdit *)(widget->cellWidget(row,column)))->setText(item.attribute(nomTableau+QString::number(row)+QString::number(column)));
                    else
                        item.setAttribute(nomTableau+QString::number(row)+QString::number(column), ((QTextEdit *)(widget->cellWidget(row,column)))->toPlainText());
                }
            }
        }
        else
        {
            QMessageBox::information(NULL,"Erreur","Tableau non trouv�");
        }
    }

    return item;

}

void MainWindow::maj_allergiesAlimentaires_1()
{
    if(ui->TE_allergiesAlimentaires_2->toPlainText() != ui->TE_allergiesAlimentaires->toPlainText())
        ui->TE_allergiesAlimentaires->setText(ui->TE_allergiesAlimentaires_2->toPlainText());
}
void MainWindow::maj_allergiesAlimentaires_2()
{
    if(ui->TE_allergiesAlimentaires_2->toPlainText() != ui->TE_allergiesAlimentaires->toPlainText())
        ui->TE_allergiesAlimentaires_2->setText(ui->TE_allergiesAlimentaires->toPlainText());
}

void MainWindow::nomEnfantMajuscules()
{
    ui->LE_enfantNom->setText(ui->LE_enfantNom->text().toUpper());
}

void MainWindow::changeState_marche()
{
    QRect test = ui->GB_enfantHandicap->geometry();
    if(ui->RB_marcheOui->isChecked())
        test.setHeight(161);
    else
        test.setHeight(128);
    ui->GB_enfantHandicap->setGeometry(test);

}
void MainWindow::changeSexe()
{
    if(ui->CB_enfantSexe->currentText()=="Feminin")
        ui->GB_enfantRegles->setHidden(false);
    else
        ui->GB_enfantRegles->setHidden(true);
}

void MainWindow::choisirPhotoEnfant()
{
    QString photoPath=QFileDialog::getOpenFileName(0, QString(), QString(), "*.jpg;*.png;*.gif;*.png");
    if(QFile::exists(photoPath))
    {
        QFileInfo info(photoPath);
        QString ext = info.suffix();
        QString id_enfant = ui->L_enfantID->text();

        if(!QDir(QDir::currentPath()+"/photosEnfants/").exists())
            QDir().mkdir(QDir::currentPath()+"/photosEnfants/");

        QFile::copy(photoPath,QDir::currentPath()+"/photosEnfants/"+id_enfant+"."+ext);
        ui->L_enfantPhoto->setPixmap(QPixmap(QDir::currentPath()+"/photosEnfants/"+id_enfant+"."+ext));
    }
}
