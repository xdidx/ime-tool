#ifndef XML_DOM_H
#define XML_DOM_H

#include <QtGui>
#include <QtXml>
#include <QWidget>
#include <QPushButton>
#include <QSpinBox>

class Xml_Dom : public QWidget
{
    Q_OBJECT

public:
    Xml_Dom();
    QStringList listeSections();
    QStringList listeMedecins();
    QList<QListWidgetItem*> listeEnfants(QString section="");
    QStringList listeVisites(QString enfant, QString medecin);
    QDomNodeList listePesees(QString enfant);
    QDomElement InfosPesee(QString enfant, QString dateVisite);
    QDomElement InfosVisite(QString enfant, QString date, QString medecin);
    QDomElement InfosEnfant(QString enfant);
    QDomDocument getDomDocument(QString fileName="");
    QString getFieldFromField(QString tagName, QString fieldWanted, QString fieldValue, QString fieldName);
    bool ajouterElement(QString noeud, QDomElement newNode);
    bool majElement(QString noeud, QDomElement newNode);
    bool supprimerElement(QString nomNoeud, QString texte, QString nomChamps1, QString nomChamps2="");
    bool majFichier(QDomDocument dom, QString fileName = "");
    QDomDocument supprimerEnfants(QDomDocument dom, QString noeudEnfant, QString colonne, QString idParent);
    bool derniereVisiteInf(QString id_enfant);

private :
    QString xmlFile;

};

#endif // XML_DOM_H
